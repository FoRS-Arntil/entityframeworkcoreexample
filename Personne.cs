using System.ComponentModel.DataAnnotations.Schema;

public class Personne {
    public int PersonneId { get; set; }
    public string Nom { get; set; }
    public List<Adresse> Adresses { get; } = new();
    [NotMapped]
    public string Secret { get; set; }
}
