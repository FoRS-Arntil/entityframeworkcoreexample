public class Adresse {
    public int AdresseId { get; set; }
    public string Rue { get; set; }
    public string Ville { get; set; }
    public string Pays { get; set; }

    public int PersonneId { get; set; }
    public Personne Personne { get; set; }
}
