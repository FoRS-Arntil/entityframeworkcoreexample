using Microsoft.EntityFrameworkCore;

public class RegistreContext : DbContext
{
    public DbSet<Personne> Personnes { get; set; }
    public DbSet<Adresse> Adresses { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlite($"Data Source=database.sqlite");

}