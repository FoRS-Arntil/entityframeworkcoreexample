﻿RegistreContext db = new RegistreContext();

Personne personne = new Personne {
    Nom = "Jean Dupont",
    Secret = "Super secret"
};

personne.Adresses.Add(new Adresse {
    Rue = "Rue du pont 12, 4000",
    Ville = "Liège",
    Pays = "Belgique"
});

personne.Adresses.Add(new Adresse {
    Rue = "Rue haut bois 14, 5000",
    Ville = "Namur",
    Pays = "Belgique"
});

db.Add(personne);
db.SaveChanges();

Console.WriteLine("Saved to the database");

db.Personnes.ToList().ForEach(pers => {
    Console.WriteLine("Personne :");
    Console.WriteLine("PersonneId : " + pers.PersonneId);
    Console.WriteLine("Nom : " + pers.Nom);
    Console.WriteLine("Secret :" + pers.Secret);
    pers.Adresses.ForEach(adresse => {
        Console.WriteLine("Adresse : ");
        Console.WriteLine("AdresseId : " + adresse.AdresseId);
        Console.WriteLine("Rue : " + adresse.Rue);
        Console.WriteLine("Ville : " + adresse.Ville);
        Console.WriteLine("Pays : " + adresse.Pays);
    });
});
